import json

from django.db import transaction
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView

from erp.forms import CompraForm
from erp.models import Producto, Compra, DetCompra


class listarComp(ListView):
    model=Compra
    template_name = 'compras/listCompra'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request,*args,**kwargs)

    def post(self,request, *args,**kwargs):
        data={}
        try:
            action= request.POST['ction']
            if action=='searchdata':
                data=[]
                for i in Compra.objects.all():

                    pass


        except Exception as e:
            pass
        return data


    pass

class agregarVenta(CreateView):
    model = Compra
    form_class = CompraForm
    template_name ='compras/agregarCompra.html'
    success_url = reverse_lazy('erp:listar_compras')
    permisition_required='erp:agregar_compras'
    url_redirect=success_url

    @method_decorator(csrf_exempt())
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request,*args,**kwargs)

    def post(self, request, *args, **kwargs):
        data={}

        try:
            action=request.POST['action']
            if action=='search_products':
                prods=Producto.objects.filter(nomProd__iconteins=request.POST['term'])[0:10]
                for i in prods:
                    items=i.toJSON()
                    items['value']=i.nomProd
                    data.apped(items)
            elif action=='add':
                with transaction.atomic():
                    comp=json.loads(request.POST['comps'])
                    compra=Compra()
                    compra.prod_id=comp['idProd']
                    compra.date_joined=comp['data_joined']
                    comp.subtotal=float(comp['subtotal'])
                    compra.total=float(comp['total'])
                    compra.save()
                    for i in compra['products']:
                        det=DetCompra()
                        det.comp_id=compra.id
                        det.idProd_id=i['id']
                        det.cant=int(i['cant'])
                        det.pComp=float(i['pComp'])
                        det.sutotal=float(i['subtotal'])
                        det.total=float(i['totl'])
                        det.save()
                    for i in compra['compra']:
                        prom=DetCompra.objects.filter()




            pass
        except Exception as e:
            data['error']=str(e)
        return JsonResponse(data, safe=False)


