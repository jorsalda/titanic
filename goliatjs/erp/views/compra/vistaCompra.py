import json

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.db.models import Avg

from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, DeleteView, UpdateView
from numpy import array

from erp.forms import CompraForm
from erp.mixins import ValidatePermissionRequiredMixin
from erp.models import Compra, DetCompra, Producto


class listarCompra(LoginRequiredMixin, ValidatePermissionRequiredMixin, ListView):
    model = DetCompra
    template_name ='compras/listCompras.html'
    permission_required = 'erp.view_compra'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request,*args,**kwargs)

    def post(self,request, *args,**kwargs):
        data={}
        try:
            action=request.POST['action']
            if action == 'searchdata':
                data=[]
                for i in Compra.objects.all():
                    data.append(i.toJSON())
            elif action == 'search_details_prod':
                data = []
                for i in DetCompra.objects.filter(comp_id=request.POST['id']):
                    data.append(i.toJSON())
            else:
                data['error']='Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context=super().get_context_data(**kwargs)
        context['title']='Listado de Compras'
        context['create_url']=reverse_lazy('erp:crear_compras')
        context['list_url']=reverse_lazy('erp:listar_compras')
        context['entity']='Compras'
        return context

class CrearCompra(LoginRequiredMixin, ValidatePermissionRequiredMixin, CreateView):
    model = Compra
    form_class = CompraForm
    template_name = 'compras/agregarCompras.html'
    success_url = reverse_lazy('erp:listar_compras')
    permission_required = 'erp.agregar_compra'
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'search_products':
                data = []
                prods = Producto.objects.filter(nomProd__icontains=request.POST['term'])[0:10]
                for i in prods:
                    item = i.toJSON()
                    item['value'] = i.nomProd

                    data.append(item)
            elif action == 'add':
                with transaction.atomic():
                    comp = json.loads(request.POST['comps'])
                    compra = Compra()
                    compra.idProv_id = comp['idProv']
                    compra.date_joined = comp['date_joined']
                    compra.total = float(comp['total'])
                    compra.save()
                    for i in comp['products']:
                        det = DetCompra()
                        det.comp_id = compra.id
                        #det.idProv_id = i['idProvs']
                        det.prod_id = i['id']
                        det.stocks = int(i['stocks'])
                        det.pComp= float(i['pComp'])
                        print(det.pComp)
                        det.cant = int(i['cant'])
                        det.can = int(i['cant'])
                        #det.iva = float(i['iva'])*det.cant*det.pComp # guardar iva calculado por cada producto
                        det.iva = float(i['iva'])    # guardar iva
                        det.ivap = float(i['ivap'])    # guardar iva por cada producto
                        det.subtotalP = float(i['subtotalP'])# guardar subtoral por producto
                        det.subtotal = float(i['subtotal']) # guardar total producto
                        det.stocksf= det.cant+det.stocks # guardar total producto
                        Sto = Producto.objects.get(id=i['id'])
                        Sto.stocks = Sto.stocks + det.cant  # se incrementa el stock
                        prome = DetCompra.objects.all().aggregate(Avg('pComp'))
                        #Sto.pComp = prome['pComp__avg']
                        print( 'promedio:',prome['pComp__avg'])
                        Sto.save()
                        det.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de una Compra'
        context['entity'] = 'Compra'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        context['det'] = []
        return context


class EditarCompra(LoginRequiredMixin, ValidatePermissionRequiredMixin, UpdateView):
    model = Compra
    form_class = CompraForm
    template_name = 'compras/agregarCompras.html'
    success_url = reverse_lazy('erp:listar_compras')
    permission_required = 'erp.change_compra'
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'search_products':
                data = []
                prods = Producto.objects.filter(nomProd__icontains=request.POST['term'])[0:10]
                for i in prods:
                    item = i.toJSON()
                    item['value'] = i.nomProd
                    data.append(item)
            elif action == 'edit':
                with transaction.atomic():
                    comp = json.loads(request.POST['comps'])
                    #compra = Compra.objects.get(pk=self.get_object().id)
                    compra = self.get_object()
                    compra.idProv_id = comp['idProv']
                    compra.date_joined = comp['date_joined']
                    compra.subtotal = float(comp['subtotal'])
                    compra.total = float(comp['total'])
                    compra.save()
                    compra.detcompra_set.all().delete()
                    for i in comp['products']:
                        det = DetCompra()
                        det.comp_id = compra.id
                        det.prod_id = i['id']
                        det.stocks = int(i['stocks'])
                        det.pComp = float(i['pComp'])
                        det.cant = int(i['cant'])
                        det.can = int(i['cant'])
                        det.iva = float(i['iva'])   # guardar iva calculado por cada producto
                        det.ivap = float(i['ivap'])   # guardar iva calculado por cada producto
                        det.subtotalP = float(i['subtotalP'])  # guardar subtoral producto
                        det.subtotal = float(i['subtotal'])  # guardar total producto
                        Sto = Producto.objects.get(id=i['id'])
                        Sto.stocks = Sto.stocks + det.cant  # se incrementa el stock
                        Sto.save()
                        det.stocksf =Sto.stocks -(det.can-det.cant)+det.cant
                        det.save()
                        for i in comp['products']:
                            prome = DetCompra.objects.all().aggregate(Avg('pComp'))
                            Sto.pComp = prome['pComp__avg']
                            Sto.save()
                        print('promedio actulizado',prome['pComp__avg'])
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def Comprajes(self):
        data = []
        try:
            for i in DetCompra.objects.filter(comp_id=self.get_object().id):
                print(i)
                item = i.prod.toJSON()
                item['cant'] = i.cant
                item['iva'] = float(i.iva)
                item['pComp'] = float(i.pComp)
                item['ivap'] = float(i.ivap)
                item['subtotal'] = float(i.subtotal)
                data.append(item)
        except:
            pass
        return data


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de una Compra'
        context['entity'] = 'Compras'
        context['list_url'] = self.success_url
        context['action'] = 'edit'
        context['det'] = json.dumps(self.Comprajes())
        #context['det'] = json.dumps(self.Comprajes())
        return context



class eliminarCompra(LoginRequiredMixin, ValidatePermissionRequiredMixin, DeleteView):
    model = Compra
    template_name = 'compras/eliminarCompra.html'
    success_url = reverse_lazy('erp:listar_compras')
    permission_required = 'erp.borrar_compra'
    url_redirect = success_url

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
           idC=self.object.id
           idsProd = DetCompra.objects.all().select_related('prod').filter(comp=idC).values_list('prod_id','cant')
           v1 = array(idsProd)
           for i in range(len(v1)):
               idPro = Producto.objects.get(pk=v1[i][0])
               idPro.stocks = idPro.stocks - (v1[i][1])
               idPro.save()
           self.object.delete()

        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de una Compra'
        context['entity'] = 'Compras'
        context['list_url'] = self.success_url
        return context

