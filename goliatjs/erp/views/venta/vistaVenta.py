import json

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.db.models import Avg
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from erp.forms import VentaForm
from erp.mixins import ValidatePermissionRequiredMixin
from django.views.generic import CreateView, ListView, DeleteView,UpdateView

from erp.models import DetVenta, Producto, Venta


class listarVentas(LoginRequiredMixin, ValidatePermissionRequiredMixin, ListView):
    model = Venta
    template_name = 'venta/listarVentas.html'
    permission_required = 'erp.view_sale'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in Venta.objects.all():
                    data.append(i.toJSON())
            elif action == 'search_details_prod':
                data = []
                for i in DetVenta.objects.filter(sale_id=self.get_object().id):
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Ventas'
        context['create_url'] = reverse_lazy('erp:crear_venta')
        context['list_url'] = reverse_lazy('erp:listar_ventas')
        context['entity'] = 'Ventas'
        return context

class crearVentas(LoginRequiredMixin, ValidatePermissionRequiredMixin, CreateView):
    model = Venta
    form_class = VentaForm
    template_name = 'venta/crearVenta.html'
    success_url = reverse_lazy('erp:listar_ventas')
    permission_required = 'erp.add_sale'
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'search_products':
                data = []
                prods = Producto.objects.filter(nomProd__icontains=request.POST['term'])[0:10]
                for i in prods:
                    item = i.toJSON()
                    item['value'] = i.nomProd
                    data.append(item)
            elif action == 'add':
                with transaction.atomic():
                    vent= json.loads(request.POST['vents'])
                    venta = Venta()
                    venta.idClie_id = vent['idClie']
                    venta.date_joined = vent['date_joined']
                    venta.total = float(vent['total'])
                    venta.save()
                    for i in vent['products']:
                        det = DetVenta()
                        det.vent_id = venta.id
                        # det.idProv_id = i['idProvs']
                        det.prod_id = i['id']
                        det.stocks = int(i['stocks'])
                        det.pVent = float(i['pVent'])
                        print('Detalle venta',det.pVent)
                        det.cant = int(i['cant'])
                        det.can = int(i['cant'])
                        # det.iva = float(i['iva'])*det.cant*det.pComp # guardar iva calculado por cada producto
                        det.iva = float(i['iva'])  # guardar iva
                        det.ivap = float(i['ivap'])  # guardar iva por cada producto
                        det.subtotalP = float(i['subtotalP'])  # guardar subtoral por producto
                        det.subtotal = float(i['subtotal'])  # guardar total producto
                        det.stocksf = det.cant + det.stocks  # guardar total producto
                        Sto = Producto.objects.get(id=i['id'])
                        Sto.stocks = Sto.stocks - det.cant  # se incrementa el stock
                        prome = DetVenta.objects.all().aggregate(Avg('pVent'))
                        # Sto.pComp = prome['pComp__avg']
                        print('promedio:', prome['pVent__avg'])
                        Sto.save()
                        det.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Creación de una Venta'
        context['entity'] = 'Ventas'
        context['list_url'] = self.success_url
        context['action'] = 'add'
        return context

class editarVentas(LoginRequiredMixin, ValidatePermissionRequiredMixin, UpdateView):
    model = Venta
    form_class = VentaForm
    template_name = 'venta/crearVenta.html'
    success_url = reverse_lazy('erp:listar_ventas')
    permission_required = 'erp.change_sale'
    url_redirect = success_url

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'search_products':
                data = []
                prods = Producto.objects.filter(nomProd__icontains=request.POST['term'])[0:10]
                for i in prods:
                    item = i.toJSON()
                    item['value'] = i.nomProd
                    data.append(item)
            elif action == 'edit':
                with transaction.atomic():
                    vent = json.loads(request.POST['vents'])
                    # venta = Venta.objects.get(pk=self.get_object().id)# otra forma de hacer la consulta de la ventas
                    venta = self.get_object()
                    venta.idClie_id = vent['idClie']
                    venta.date_joined = vent['date_joined']
                    venta.subtotal = float(vent['subtotal'])
                    venta.total = float(vent['total'])
                    venta.save()
                    venta.detventa_set.all().delete()
                    for i in vent['products']:
                        det = DetVenta()
                        det.vent_id = venta.id
                        det.prod_id = i['id']
                        det.stocks = int(i['stocks'])
                        det.pVent = float(i['pComp'])
                        det.cant = int(i['cant'])
                        det.can = int(i['cant'])
                        det.iva = float(i['iva'])  # guardar iva calculado por cada producto
                        det.ivap = float(i['ivap'])  # guardar iva calculado por cada producto
                        det.subtotalP = float(i['subtotalP'])  # guardar subtoral producto
                        det.subtotal = float(i['subtotal'])  # guardar total producto
                        Sto = Producto.objects.get(id=i['id'])
                        Sto.stocks = Sto.stocks + det.cant  # se incrementa el stock
                        Sto.save()
                        det.stocksf = Sto.stocks - (det.can - det.cant) + det.cant
                        det.save()
                        for i in comp['products']:
                            prome = DetVenta.objects.all().aggregate(Avg('pVent'))
                            Sto.pComp = prome['pVent__avg']
                            Sto.save()
                        print('promedio actulizado', prome['pVent__avg'])
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_details_product(self):
        data = []
        try:
            for i in DetVenta.objects.filter(sale_id=self.get_object().id):
                item = i.prod.toJSON()
                item['cant'] = i.cant
                data.append(item)
        except:
            pass
        return data


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de una Venta'
        context['entity'] = 'Ventas'
        context['list_url'] = self.success_url
        context['action'] = 'edit'
        context['det'] = self.get_details_product()
        return context

class eliminarVentas(LoginRequiredMixin, ValidatePermissionRequiredMixin, DeleteView):
    model = Venta
    template_name = 'venta/eliminarVenta.html'
    success_url = reverse_lazy('erp:listar_ventas')
    permission_required = 'erp.delete_sale'
    url_redirect = success_url

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de una Venta'
        context['entity'] = 'Ventas'
        context['list_url'] = self.success_url
        return context
