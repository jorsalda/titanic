from datetime import datetime

from django.db import models
from django.forms import model_to_dict

from goliatjs.settings import MEDIA_URL, STATIC_URL
from erp.choices import gender_choices


class Categoria(models.Model):
    name = models.CharField(max_length=150, verbose_name='Categoria', unique=True)
    desc = models.CharField(max_length=500, null=True, blank=True, verbose_name='Descripción')

    def __str__(self):
        return self.name

    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'
        ordering = ['id']

class Producto(models.Model):
    nomProd = models.CharField(max_length=150, verbose_name='Nombrejes', unique=True)
    idCat = models.ForeignKey(Categoria, on_delete=models.CASCADE, verbose_name='Categoría')
    image = models.ImageField(upload_to='producto/%Y/%m/%d', null=True, blank=True, verbose_name='Imagen')
    stocks = models.IntegerField(default=0,  verbose_name='Stock')
    pComp = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    pVent = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)


    def __str__(self):
        return self.nomProd


    def toJSON(self):
        item = model_to_dict(self)
        item['idCat'] = self.idCat.toJSON()
        item['image'] = self.get_image()
        item['stocks'] = format(self.stocks)
        item['pComp'] = format(self.pComp, '.2f')
        item['pVent'] = format(self.pVent, '.2f')

        return item

    def get_image(self):
        if self.image:
            return '{}{}'.format(MEDIA_URL, self.image)
        return '{}{}'.format(STATIC_URL, 'img/empty.png')

    class Meta:
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'
        ordering = ['id']

class Cliente(models.Model):
    nombClie = models.CharField(max_length=150, verbose_name='Nombres')
    apeClie = models.CharField(max_length=150, verbose_name='Apellidos')
    nit_Ced = models.CharField(max_length=10, unique=True, verbose_name='Dni')
    date_birthday= models.DateField(default=datetime.now, verbose_name='Fecha de nacimiento')
    direccion = models.CharField(max_length=150, null=True, blank=True, verbose_name='Dirección')
    telefono= models.CharField(max_length=20, null=True, blank=True, verbose_name='Telefono')
    gender = models.CharField(max_length=10, choices=gender_choices, default='male', verbose_name='Sexo')

    def __str__(self):
        return self.nombClie

    def toJSON(self):
        item = model_to_dict(self)
        item['gender'] = {'id': self.gender, 'name': self.get_gender_display()}
        item['date_birthday'] = self.date_birthday.strftime('%Y-%m-%d')
        return item

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'
        ordering = ['id']

class Proveedor(models.Model):
    nombProv = models.CharField(max_length=150, verbose_name='Nombres')
    apeProv = models.CharField(max_length=150, verbose_name='Apellidos')
    nitCed = models.CharField(max_length=10, unique=True, verbose_name='Dni')
    direccion = models.CharField(max_length=10, unique=True, verbose_name='Dni')
    date_birthday = models.DateField(default=datetime.now, verbose_name='Fecha de nacimiento')
    telefono = models.CharField(max_length=150, null=True, blank=True, verbose_name='Dirección')
    gender = models.CharField(max_length=10, choices=gender_choices, default='male', verbose_name='Sexo')

    def __str__(self):
        return self.nombProv

    def toJSON(self):
        item = model_to_dict(self)
        item['gender'] = {'id':self.gender, 'name':self.get_gender_display()}
        item['date_birthday'] = self.date_birthday.strftime('%Y-%m-%d')
        return item

    class Meta:
        verbose_name = 'Proveedor'
        verbose_name_plural = 'Proveedores'
        ordering = ['id']

class Compra(models.Model):
    idProv = models.ForeignKey(Proveedor, on_delete=models.CASCADE)
    date_joined = models.DateField(default=datetime.now)
    total = models.IntegerField(default=0, verbose_name='cantidad')

    def __str__(self):
        #return self.prove.nombProv
        return self.idProv.nombProv

    def toJSON(self):
        item = model_to_dict(self)
        item['idProv'] = self.idProv.toJSON()
        item['date_joined'] = self.date_joined.strftime('%Y-%m-%d')
        item['total'] = format(self.total, '.2f')
        item['det'] = [i.toJSON() for i in self.detcompra_set.all()]#¿De dónde viene?
        return item

    class Meta:
        verbose_name = 'Compra'
        verbose_name_plural = 'Compras'
        ordering = ['id']

class DetCompra(models.Model):
    comp = models.ForeignKey(Compra, on_delete=models.CASCADE)
    prod = models.ForeignKey(Producto, on_delete=models.CASCADE)
    stocks = models.IntegerField(default=0, verbose_name='stocks')
    pComp = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    cant = models.IntegerField(default=0, verbose_name='cantidad')
    can = models.IntegerField(default=0, verbose_name='cantidad')
    iva = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    ivap = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    subtotalP = models.DecimalField(default=0.00, max_digits=29, decimal_places=2)
    subtotal = models.DecimalField(default=0.00, max_digits=29, decimal_places=2)
    stocksf =  models.IntegerField(default=0, verbose_name='stocksf')



    def __str__(self):
        return self.prod.nomProd

    def toJSON(self):
        item = model_to_dict(self, exclude=['comps'])
        item['prod'] = self.prod.toJSON()
        item['stocks'] = format(self.stocks)
        item['pComp'] = format(self.pComp, '.2f')
        item['cant'] = format(self.cant)
        item['can'] = format(self.can)
        item['iva'] = format(self.iva, '.2f')
        item['ivap'] = format(self.ivap, '.2f')
        item['subtotalP'] = format(self.subtotalP, '.2f')
        item['subtotal'] = format(self.subtotal, '.2f')
        item['stocksf'] = format(self.stocksf)
        return item

    class Meta:
        verbose_name = 'Detalle de Compra'
        verbose_name_plural = 'Detalle de Compra'
        ordering = ['id']


class Venta(models.Model):
    idClie = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    date_joined = models.DateField(default=datetime.now)
    total = models.IntegerField(default=0, verbose_name='cantidad')

    def __str__(self):
        #return self.prove.nombProv
        return self.idClie.nombClie

    def toJSON(self):
        item = model_to_dict(self)
        item['idClie'] = self.idClie.toJSON()
        item['date_joined'] = self.date_joined.strftime('%Y-%m-%d')
        item['total'] = format(self.total, '.2f')
        item['det'] = [i.toJSON() for i in self.detventa_set.all()]
        return item

    class Meta:
        verbose_name = 'Venta'
        verbose_name_plural = 'Ventas'
        ordering = ['id']

class DetVenta(models.Model):
    vent = models.ForeignKey(Venta, on_delete=models.CASCADE)
    prod = models.ForeignKey(Producto, on_delete=models.CASCADE)
    stocks = models.IntegerField(default=0, verbose_name='stocks')
    pVent = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    cant = models.IntegerField(default=0, verbose_name='cantidad')
    can = models.IntegerField(default=0, verbose_name='cantidad')
    iva = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    ivap = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    subtotalP = models.DecimalField(default=0.00, max_digits=29, decimal_places=2)
    subtotal = models.DecimalField(default=0.00, max_digits=29, decimal_places=2)
    stocksf =  models.IntegerField(default=0, verbose_name='stocksf')



    def __str__(self):
        return self.prod.nomProd

    def toJSON(self):
        item = model_to_dict(self, exclude=['vents'])
        item['prod'] = self.prod.toJSON()
        item['stocks'] = format(self.stocks)
        item['pVent'] = format(self.pVent, '.2f')
        item['cant'] = format(self.cant)
        item['can'] = format(self.can)
        item['iva'] = format(self.iva, '.2f')
        item['ivap'] = format(self.ivap, '.2f')
        item['subtotalP'] = format(self.subtotalP, '.2f')
        item['subtotal'] = format(self.subtotal, '.2f')
        item['stocksf'] = format(self.stocksf)
        return item

    class Meta:
        verbose_name = 'Detalle de Venta'
        verbose_name_plural = 'Detalle de Ventas'
        ordering = ['id']
