from datetime import datetime

from django.forms import *

from erp.models import Compra, Proveedor, Producto, Categoria, Cliente, Venta
from erp.views import venta


class CategoriaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['name'].widget.attrs['autofocus'] = True

    class Meta:
        model = Categoria
        fields = '__all__'
        widgets = {
            'name': TextInput(
                attrs={
                    'placeholder': 'Ingrese un nombre',
                }
            ),
            'desc': Textarea(
                attrs={
                    'placeholder': 'Ingrese un nombre',
                    'rows': 3,
                    'cols': 3
                }
            ),
        }

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

class ProductoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['nomProd'].widget.attrs['autofocus'] = True

    class Meta:
        model = Producto
        exclude=['stocks','pComp','pVent']
        #fields = '__all__'
        widgets = {
            'nomProd': TextInput(
                attrs={
                    'placeholder': 'Ingrese un nombre',
                }
            ),
            'idCat': Select(
                attrs={
                    'class': 'select2',
                    'style': 'width: 100%'
                }
            ),

        }


    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

class ProveedorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['nombProv'].widget.attrs['autofocus'] = True

    class Meta:
        model = Proveedor
        fields = '__all__'
        widgets = {
            'nombProv': TextInput(
                attrs={
                    'placeholder': 'Ingrese nombres del Proveedor',
                }
            ),
            'apeProve': TextInput(
                attrs={
                    'placeholder': 'Ingrese  apellidos del Proveedor',
                }
            ),
            'nit_Ced': TextInput(
                attrs={
                    'placeholder': 'Ingrese dni del proveedor',
                }
            ),
            'date_birthday': DateInput(format='%Y-%m-%d',
                                       attrs={
                                           'value': datetime.now().strftime('%Y-%m-%d'),
                                       }
                                       ),
            'diereccion': TextInput(
                attrs={
                    'placeholder': 'Ingrese la  dirección del proveedor',
                }
            ),
            'telefono': TextInput(
                attrs={
                    'placeholder': 'Ingrese la  dirección del proveedor',
                }
            ),
            'gender': Select()
        }

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

    # def clean(self):
    #     cleaned = super().clean()
    #     if len(cleaned['name']) <= 50:
    #         raise forms.ValidationError('Validacion xxx')
    #         # self.add_error('name', 'Le faltan caracteres')
    #     return cleaned

class ClienteForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['nombClie'].widget.attrs['autofocus'] = True

    class Meta:
        model = Cliente
        fields = '__all__'
        widgets = {
            'nombClie': TextInput(
                attrs={
                    'placeholder': 'Ingrese nombres del Proveedor',
                }
            ),
            'apeClien': TextInput(
                attrs={
                    'placeholder': 'Ingrese  apellidos del Proveedor',
                }
            ),
            'nit_Ced ': TextInput(
                attrs={
                    'placeholder': 'Ingrese dni del proveedor',
                }
            ),
            'date_birthday': DateInput(format='%Y-%m-%d',
                                       attrs={
                                           'value': datetime.now().strftime('%Y-%m-%d'),
                                       }
                                       ),
            'direccion': TextInput(
                attrs={
                    'placeholder': 'Ingrese la  dirección del proveedor',
                }
            ),'telefono': TextInput(
                attrs={
                    'placeholder': 'Ingrese la  dirección del proveedor',
                }
            ),
            'gender': Select()
        }

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

    # def clean(self):
    #     cleaned = super().clean()
    #     if len(cleaned['name']) <= 50:
    #         raise forms.ValidationError('Validacion xxx')
    #         # self.add_error('name', 'Le faltan caracteres')
    #     return cleaned

class CompraForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    class Meta:
        model = Compra
        fields = '__all__'
        widgets = {
            'idProv': Select(attrs={
                'class': 'form-control select2',
                'style': 'width: 100%'
            }),
            'idProd': Select(attrs={
                'class': 'form-control select2',
                'style': 'width: 100%'
            }),
            'date_joined': DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'date_joined',
                    'data-target': '#date_joined',
                    'data-toggle': 'datetimepicker'
                }
            ),
            'stocks': TextInput(attrs={
                'class': 'form-control',
            }),

            'total': TextInput(attrs={
                'class': 'form-control',
            }),

        }

class VentaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    class Meta:
        model = Venta
        fields = '__all__'
        widgets = {
            'idClie': Select(attrs={
                'class': 'form-control select2',
                'style': 'width: 100%'
            }),
            'date_joined': DateInput(
                format='%Y-%m-%d',
                attrs={
                    'value': datetime.now().strftime('%Y-%m-%d'),
                    'autocomplete': 'off',
                    'class': 'form-control datetimepicker-input',
                    'id': 'date_joined',
                    'data-target': '#date_joined',
                    'data-toggle': 'datetimepicker'
                }
            ),'stocks': TextInput(attrs={
                'class': 'form-control',
            }),
            'pVent': TextInput(attrs={
                 'readonly': True,
                'class': 'form-control',
            }),
            'cant': TextInput(attrs={
                'class': 'form-control',
            }),
            'subtotal': TextInput(attrs={
                'readonly': True,
                'class': 'form-control',
            }),
            'iva': TextInput(attrs={
                'class': 'form-control',
            }),
            'total': TextInput(attrs={
                'class': 'form-control',
            }),

        }

