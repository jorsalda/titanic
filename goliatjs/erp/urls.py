from django.urls import path

from erp.views.categoria.vistaCategoria import *
from erp.views.cliente.vistaCliente import *


from erp.views.dashboard.views import DashboardView
#from erp.views.producto.views import *
from erp.views.producto.vistaProducto import *
from erp.views.proveedor.vistaProveedor import listarProveedor, agregarProveedor, ProveedorDeleteView
#from erp.views.venta.views import *
from erp.views.venta.vistaVenta import *
#from erp.views.venta.views import *
from erp.views.compra.vistaCompra import *
#from erp.views.compra.views import *

from erp.views.tests.views import TestView

app_name = 'erp'

urlpatterns = [
    # categoria
    path('categoria/list/', listarCategoria.as_view(), name='category_list'),
    path('categoria/add/', crearCategoria.as_view(), name='category_create'),
    path('categoria/editar/<int:pk>/',editarCategoria.as_view(), name='editar_Categoria'),
    path('categoria/eliminar/<int:pk>/',eliminarCategoria.as_view(), name='eliminar_Categoria'),

    # proveedor
    path('listar/proveedor/', listarProveedor.as_view(), name='listar_proveedor'),
    path('agregar/proveedor/', agregarProveedor.as_view(), name='agregar_proveedor'),
    path('eliminar/proveedor/<int:pk>/', ProveedorDeleteView.as_view(), name='proveedor_elminnar'),

    # cliente
    path('listar/cliente/', listarCliente.as_view(), name='listar_Cliente'),
    path('agregar/cliente/', agregarCliente.as_view(), name='agregar_clente'),
    path('editar/cliente/<int:pk>/', editarCliente.as_view(), name='editar_cliente'),
    path('eliminar/cliente/<int:pk>/', eliminarCliente.as_view(), name='eliminar_cliente'),


    # producto
    path('producto/list/', listarProducto.as_view(), name='product_list'),
    path('producto/add/', agregarProducto.as_view(), name='product_create'),
    path('producto/editar/<int:pk>/', actualizarProducto.as_view(), name='editarProducto'),
    path('producto/eliminar/<int:pk>/', eliminarProducto.as_view(), name='eliminarProducto'),

    # home
    path('dashboard/', DashboardView.as_view(), name='dashboard'),

    # test
    path('test/', TestView.as_view(), name='test'),
    # venta

    # compras
    path('Listar/compra/', listarCompra.as_view(), name='listar_compras'),
    path('Agregar/compra/', CrearCompra.as_view(), name='crear_compras'),
    path('compra/borrarCompra/<int:pk>/', eliminarCompra.as_view(), name='borrar_compra'),
    path('compra/editarComp/<int:pk>/', EditarCompra.as_view(), name='editar_compra'),

    # ventas
    path('Listar/venta/', listarVentas.as_view(), name='listar_ventas'),
    path('crear/venta/', crearVentas.as_view(), name='crear_venta'),
    path('eliminar/venta/<int:pk>/', eliminarVentas.as_view(), name='eliminar_venta'),
    path('editar/venta/<int:pk>/', editarVentas.as_view(), name='editar_venta'),
]
