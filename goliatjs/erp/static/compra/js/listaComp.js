var tblSale;

function format(d) {
    console.log(d);
    var html = '<table class="table"  style="color:yellow" >';
    html += '<thead class="thead-dark">';
    html += '<tr><th scope="col">Producto</th>';
    html += '<th scope="col" >Stocks inicial</th>';
    html += '<th scope="col">Cantidad Comprada</th>';
    html += '<th scope="col">Precio Compra</th>';
    html += '<th scope="col">Sutotal Producto</th>';
    html += '<th scope="col">iva</th>';
    html += '<th scope="col">iva Cal</th>';
    html += '<th scope="col">Total producto</th>';
    html += '<th scope="col">Stock final</th></tr>';

    html += '</thead>';
    html += '<tbody style="background-color:green;">';
    $.each(d.det, function (key, value) {
        html+='<tr>'
        html+='<td>'+value.prod.nomProd+'</td>'
        html+='<td>'+value.stocks+'</td>'
        html+='<td>'+value.cant+'</td>'
        html+='<td>'+value.pComp+'</td>'
        html+='<td>'+value.subtotalP+'</td>'
        html+='<td>'+value.iva+'</td>'
        html+='<td>'+value.ivap+'</td>'
        html+='<td>'+value.subtotal+'</td>'
        html+='<td>'+value.stocksf+'</td>'
        html+='</tr>';
    });
    html += '</tbody>';
    return html;
}
$(function () {
    tblSale = $('#data').DataTable({
        //responsive: true,
        scrollX: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata',
            },
            dataSrc: ""
        },
        columns: [
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {"data": "idProv.nombProv"},
            {"data": "date_joined"},
            {"data": "total"},
            {"data": "id"},

        ],
        columnDefs: [
            {
                targets: [-2],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    return '$' + parseFloat(data).toFixed(2);
                }
            },
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    var buttons = '<a href="/erp/compra/borrarCompra/' + row.id + '/" class="btn btn-danger btn-xs btn-flat"><i class="fas fa-trash-alt"></i></a> ';
                    buttons+= '<a href="/erp/compra/editarComp/' + row.id + '/" class="btn btn-warning btn-xs btn-flat"><i class="fas fa-edit"></i></a> ';
                    buttons += '<a rel="details" class="btn btn-success btn-xs btn-flat"><i class="fas fa-search"></i></a> ';
                       return buttons;
                }
            },
        ],
        initComplete: function (settings, json) {

        }
    });

    $('#data tbody')
        .on('click', 'a[rel="details"]', function () {
            var tr = tblSale.cell($(this).closest('td, li')).index();
            var data = tblSale.row(tr.row).data();
            console.log(data);
            $('#tblDet').DataTable({
                responsive: true,
                autoWidth: false,
                destroy: true,
                deferRender: true,
                // data: data.det,
                ajax: {
                    url: window.location.pathname,
                    type: 'POST',
                    data: {
                        'action': 'search_details_prod',
                        'id': data.id
                    },
                    dataSrc: ""
                },

                columns: [
                    {"data": "prod.nomProd"},
                    //{"data": "prod.idCat.name"},
                    {"data": "stocks"},
                    {"data": "cant"},
                    {"data": "pComp"},
                    {"data": "subtotalP"},
                    {"data": "iva"},
                    {"data": "ivap"},
                    {"data": "subtotal"},
                    {"data": "stocksf"},
                ],
                columnDefs: [
                        {
                        targets: [-1],
                        class: 'text-center',
                        render: function (data, type, row) {
                            return data;
                        }
                    },
                    {
                        targets: [-2],
                        class: 'text-center',
                        render: function (data, type, row) {
                            return '$' + parseFloat(data).toFixed(2);
                        }
                    },
                    {
                        targets: [-3],
                        class: 'text-center',
                        render: function (data, type, row) {
                            return '$' + parseFloat(data).toFixed(2);
                        }
                    },
                    {
                        targets: [-4],
                        class: 'text-center',
                        render: function (data, type, row) {
                           return '$' + parseFloat(data).toFixed(2);
                        }
                    },
                    {
                        targets: [-5],
                        class: 'text-center',
                        render: function (data, type, row) {
                            return '$' + parseFloat(data).toFixed(2);
                        }
                    },
                     {
                        targets: [-6],
                        class: 'text-center',
                        render: function (data, type, row) {
                            return '$' + parseFloat(data).toFixed(2);
                        }
                    },
                     {
                        targets: [-7],
                        class: 'text-center',
                        render: function (data, type, row) {
                            return data;
                        }
                    },
                    {
                        targets: [-8],
                        class: 'text-center',
                        render: function (data, type, row) {
                            return data;
                        }
                    },
                ],
                initComplete: function (settings, json) {

                }
            });

            $('#myModelDet').modal('show');
        })
        .on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = tblSale.row(tr);
            if (row.child.isShown()) {
                row.child.hide();
                tr.removeClass('shown');
            } else {
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });

});