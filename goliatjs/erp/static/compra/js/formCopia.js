var tblProducts;
var comps= {
    items: {
    idProv: '',
    date_joined: '',
    stocks:0,
    iva:0.12,
    ivac:0.00,
    cant: 0,
    pComp: 0.00,
    subtotalP: 0.00,
    subtotal: 0.00,
    total: 0.00,
    products: []
    },

    calculate_invoice: function () {
        var subtotal = 0.00;
        var ivac = 0.00;
        var subtotalP = 0.00;
        $.each(this.items.products, function (pos, dict) {
            dict.pos=pos;
           dict.subtotal = (dict.cant * dict.pComp) +(dict.cant * dict.pComp *dict.iva);//cambia el valor que tiene subtotal el diccionarios
           iva = (dict.cant * dict.pComp*dict.iva);
           subtotal += dict.subtotal;
           dict.subtotalP = (dict.cant * dict.pComp)
           ivac+=iva;
        });
        this.items.iva = iva;
        this.items.subtotal = subtotal;
        this.items.total = this.items.subtotal;

        $('input[name="subTotalComp"]').val(this.items.subtotal.toFixed(2)-ivac.toFixed(2));//mostrar el dato subtotal por pantalla
        $('input[name="ivacalc"]').val(ivac.toFixed(2));//mostrar el dato ivacalculado por pantalla
        $('input[name="total"]').val(this.items.total.toFixed(2));//mostrar el dato total por pantalla
          },

    add: function (item) {
        this.items.products.push(item);
        this.list();
    },


     list: function () {
        this.calculate_invoice();
        tblProducts = $('#tblProducts').DataTable({
            responsive: true,
            autoWidth: false,
            destroy: true,
            data: this.items.products,
            columns: [
                {"data": "id"},
                {"data": "nomProd"},
                {"data": "stocks"},
                {"data": "cant"},
                {"data": "pComp"},
                {"data": "iva"},
                {"data": "subtotal"},
            ],
            columnDefs: [
                {
                    targets: [0],
                    class: 'text-center',
                    orderable: false,
                    render: function (data, type, row) {
                        return '<a rel="remove" class="btn btn-danger btn-xs btn-flat" style="color: white;"><i class="fas fa-trash-alt"></i></a>';
                    }
                },
                {
                    targets: [-5],//stocks
                    class: 'text-center',
                    orderable: false,
                    render: function (data, type, row) {
                        return  + parseFloat(data).toFixed(2);
                    }
                },
                {
                    targets: [-4], //cantidad
                    class: 'text-center',
                    orderable: false,
                    render: function (data, type, row) {
                        return '<input type="text" name="cant" class="form-control form-control-sm input-sm" autocomplete="off" value="' + row.cant + '">';
                    }
                },
                {
                    targets: [-3],//precio compra
                    class: 'text-center',
                    orderable: false,
                    render: function (data, type, row) {
                        return '<input type="text" name="pComp" class="form-control form-control-sm input-sm" value="' + row.pComp+ '">';
                    }
                },

                {
                    targets: [-2],//iva
                    class: 'text-center',
                    orderable: false,
                    render: function (data, type, row) {
                        return '<input type="text" name="iva" class="form-control form-control-sm input-sm" autocomplete="off" value="' + row.iva + '">';
                    }
                },
                {
                    targets: [-1],//subtotal
                    class: 'text-center',
                    orderable: false,
                     render: function (data, type, row) {
                        return '<input type="text" name="subtotal" class="form-control form-control-sm input-sm" value="' + row.subtotal+ '">';
                    }
                },

            ],

             rowCallback(row, data, displayNum, displayIndex, dataIndex) {

               $(row).find('input[name="cant"]').TouchSpin({
                    min: 1,
                    max: 1000000000,
                    step: 1
                });

                $(row).find('input[name="iva"]').TouchSpin({
                    min: 0,
                    max: 100,
                    step: 0.01,
                    decimals: 2,
                    boostat: 5,
                    maxboostedstep: 10,
                    postfix: '%'
                }).on('change', function () {
                    comps.calculate_invoice();
                }).val();
            },

            initComplete: function (settings, json) {

            }
        });
    },
};

$(function () {
    $('.select2').select2({
        theme: "bootstrap4",
        language: 'es'
    });

    $('#date_joined').datetimepicker({
        format: 'YYYY-MM-DD',
        date: moment().format("YYYY-MM-DD"),
        locale: 'es',
        //minDate: moment().format("YYYY-MM-DD")
    });

    // search products

    $('input[name="search"]').autocomplete({
        source: function (request, response) {
            $.ajax({
                url: window.location.pathname,
                type: 'POST',
                data: {
                    'action': 'search_products',
                    'term': request.term
                },
                dataType: 'json',
            }).done(function (data) {
                response(data);
            }).fail(function (jqXHR, textStatus, errorThrown) {
                //alert(textStatus + ': ' + errorThrown);
            }).always(function (data) {
            });
        },
        delay: 500,
        minLength: 1,
        select: function (event, ui) {
            event.preventDefault();
            console.clear();
            ui.item.cant = 1;
            ui.item.iva=0.12;
            ui.item.ivac=0.00;
            ui.item.pComp =0.00;
            ui.item.subtotalP = 0.00;
            ui.item.subtotal = 0.00;
            ui.item.total = 0.00;
            //console.log(comps.items);
            comps.add(ui.item);
            $(this).val('');
        }
    });

   $('.btnEliminarTodo').on('click', function(){
   if (comps.items.products.length===0) return false;
   alert_action('Notificación', '¿Estas seguro todos los detalles?', function () {
                comps.items.products=[];
                comps.list();
            });
   });

    // evento  cantidad
         $('#tblProducts tbody').on('click', 'a[rel="remove"]', function () {
            var tr = tblProducts.cell($(this).closest('td, li')).index();
            alert_action('Notificación', '¿Estas seguro de eliminar el producto de tu detalle?', function () {
                comps.items.products.splice(tr.row, 1);
                comps.list();
            });
        }).on('change','input[name="cant"]' , function () {
        console.clear();
        var cant=parseInt($(this).val());
        var tr = tblProducts.cell($(this).closest('td, li')).index();
        comps.items.products[tr.row].cant=cant;
       // console.log(cant)
        comps.calculate_invoice();
       $('td:eq(6)', tblProducts.row(tr.row).node()).html('$' + comps.items.products[tr.row].subtotal.toFixed(2));

    }).on('change keyup','input[name="pComp"]' ,function () {
        console.clear();
        var pComp=parseInt($(this).val());
        var tr = tblProducts.cell($(this).closest('td, li')).index();
        comps.items.products[tr.row].pComp=pComp;
        //console.log(pComp);
        comps.calculate_invoice();
        $('td:eq(6)', tblProducts.row(tr.row).node()).html('$' + comps.items.products[tr.row].subtotal.toFixed(2));
    }).on('change keyup','input[name="iva"]' , function () {
        console.clear();
        var iva=parseFloat($(this).val());
        var tr = tblProducts.cell($(this).closest('td, li')).index();
        comps.items.products[tr.row].iva=iva;
        comps.calculate_invoice();
       $('td:eq(6)', tblProducts.row(tr.row).node()).html('$' + comps.items.products[tr.row].subtotal.toFixed(2));

    });

    $('.btnClearSearch').on('click', function () {
        $('input[name="search"]').val('').focus();
    });
    // event submit
    $('form').on('submit', function (e) {
        e.preventDefault();
        if(comps.items.products.length === 0){
            message_error('Debe al menos tener un item en su detalle de venta');
            return false;
        }
        comps.items.date_joined = $('input[name="date_joined"]').val();
        comps.items.idProv = $('select[name="idProv"]').val();
        var parameters = new FormData();
        parameters.append('action', $('input[name="action"]').val());
        parameters.append('comps', JSON.stringify(comps.items));
        submit_with_ajax(window.location.pathname, 'Notificación', '¿Estas seguro de realizar la siguiente acción?', parameters, function () {
            location.href = '/erp/Listar/compra/';
        });
    });

    comps.list();
});